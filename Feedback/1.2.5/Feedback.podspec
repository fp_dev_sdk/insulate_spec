Pod::Spec.new do |s|
  s.name = "Feedback"
  s.version = "1.2.5"
  s.summary = "A short description of Feedback."
  s.license = {"type"=>"MIT", "file"=>"LICENSE"}
  s.authors = {"liyan"=>"liyan@gomo.com"}
  s.homepage = "https://github.com/"
  s.description = "TODO: Add long description of the pod here."
  s.source ={ :http => 'https://raw.githubusercontent.com/devFP-byte/insult_framework/main/FeedbackSDK/1.2.5/FeedbackSDK.zip'}
  s.ios.deployment_target    = '9.0'
  s.ios.vendored_framework   = 'ios/Feedback.framework'
  s.resources = 'ios/Feedback.bundle'
  s.frameworks = 'UIKit', 'SystemConfiguration', 'CoreTelephony', 'StoreKit', "Photos"
  s.libraries = 'resolv'
  s.dependency 'AFNetworking'
  s.dependency 'Masonry'
  s.dependency 'AliyunOSSiOS'
  s.dependency 'MBProgressHUD'
  s.dependency 'SDWebImage'
  s.pod_target_xcconfig = { 'VALID_ARCHS' => 'x86_64 armv7 arm64' }
end
